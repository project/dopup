(function ($, Drupal, drupalSettings, cookies) {

    Drupal.behaviors.dopup = {
      attach: function (context, settings) {
        $(document).ready( function () {
          let count = [];
          // Bind to every popup.
          const popups = once('dopup', '.dopup__block', context);
          popups.forEach( function (popup, index) {
            // $(popup).css({
            //   'width': '90%',
            // });
            const block_machine_id = $(popup).attr('data-machine-id');
            if (typeof cookies.get('dopup_' + block_machine_id) === 'undefined') {
              count.block_machine_id = settings.dopup['dopupblock']['dopupblock_number']
            }
            else {
              count.block_machine_id = cookies.get('dopup_' + block_machine_id);
            }
            if (count.block_machine_id > 0) {
              cookies.set('dopup_' + block_machine_id, count.block_machine_id - 1);
            }
            else {
              return;
            }
            if ($(popup).is(':visible')) {
              $(document).unbind('keydown').on('keydown', function (event) {
                if (event.keyCode === 27) {
                  $(popup).hide();
                }
              });
            }
            // Determine position of popup.
            switch (drupalSettings.dopup[block_machine_id][block_machine_id + '_position']) {
              case 'center':
                $(popup).center();
                break;

              case 'top-left':
                $(popup).topLeft();
                break;

              case 'top-right':
                $(popup).topRight();
                break;

              case 'bottom-left':
                $(popup).bottomLeft();
                break;

              case 'bottom-right':
                $(popup).bottomRight();
                break;

              case 'custom':
                $(popup).calculatePosition(
                  drupalSettings.dopup[block_machine_id][block_machine_id + '_custom_left'],
                  drupalSettings.dopup[block_machine_id][block_machine_id + '_custom_top']
                );
                break;

              case 'default':
                $(popup).bottomRight();
                break;
            }

            // Determine time to display popup.
            switch (drupalSettings.dopup[block_machine_id][block_machine_id + '_when']) {
              case 'five':
                $(popup).showAfter(5 * 1000);
                break;

              case 'now':
                $(popup).showAfter(10);
                break;

              case 'bottom':
                $(popup).checkScroll(80);
                break;

              case 'middle':
                $(popup).checkScroll(45);
                break;

              case 'custom':
                if (drupalSettings.dopup[block_machine_id][block_machine_id + '_custom_when'].length !== 0) {
                  $(popup).showAfter(drupalSettings.dopup[block_machine_id][block_machine_id + '_custom_when']);
                }
                if (drupalSettings.dopup[block_machine_id][block_machine_id + '_custom_where'].length !== 0) {
                  $(popup).checkScroll(drupalSettings.dopup[block_machine_id][block_machine_id + '_custom_where']);
                }
                break;

              case 'default':
                $(popup).showAfter(5 * 1000);
                break;
            }
          });
          $('div.dopup-close').each( function () {
            $(this).unbind('click').on('click', function () {
              $(this).parents('.dopup__block').hide();
            });
          });
        });

        // Time functions.
        $.fn.showAfter = function (time) {
          const $this = $(this);
          setTimeout(function () {
            $this.show();
          }, time);
        };
        $.fn.checkScroll = function (percent) {
          const $this = $(this);
          $(document).scroll( function () {
            // grab the scroll amount and the window height
            const scrollAmount = $(window).scrollTop() + window.innerHeight;
            const documentHeight = $(document).outerHeight();
            // calculate the percentage the user has scrolled down the page
            const scrollPercent = (scrollAmount / documentHeight) * 100;
            if(scrollPercent > percent) {
              $this.show();
            }

          });
        };

        // Position functions.
        $.fn.calculatePosition = function (_left, _top) {
          $(this).css({
            'top': (_top / 100) * window.innerHeight + 'px',
            'left': (_left / 100) * window.innerWidth + 'px'
          });
        };
        $.fn.center = function () {
          $(this).css({
            'top': ($(window).height() / 2 - $(this).height() / 2) + 'px',
            'left': ($(window).width() / 2 - $(this).width() / 2) + 'px',
          });
        };
        $.fn.bottomLeft = function () {
          $(this).css({
            'bottom': '25px',
            'left': '25px',
          });
        };
        $.fn.bottomRight = function () {
          $(this).css({
            'bottom': '25px',
            'right': '25px',
          });
        };
        $.fn.topRight = function () {
          $(this).css({
            'top': '25px',
            'right': '25px',
          });
        };
        $.fn.topLeft = function () {
          $(this).css({
            'top': '25px',
            'left': '25px',
          });
        };
      }
    };

  })(jQuery, Drupal, drupalSettings, window.Cookies);
