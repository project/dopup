<?php

namespace Drupal\dopup\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use GuzzleHttp\Cookie\SetCookie;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides an example block.
 *
 * @Block(
 *   id = "dopup_block",
 *   admin_label = @Translation("Dopup Block"),
 *   category = @Translation("Dopup"),
 * )
 */
class DopupBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Entity type manager service to load webform.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Current route match service.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $routeMatch;

  /**
   * To load Dopup configs.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $configFactory;

  /**
   * Reuqest stack service.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  private $requestStack;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entityTypeManager, CurrentRouteMatch $routeMatch, ConfigFactoryInterface $configFactory, RequestStack $requestStack) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entityTypeManager;
    $this->routeMatch = $routeMatch;
    $this->configFactory = $configFactory;
    $this->requestStack = $requestStack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('current_route_match'),
      $container->get('config.factory'),
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'webform' => '',
      'machine_name' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['webform'] = [
      '#title' => 'Select Webform',
      '#type' => 'textfield',
      '#autocomplete_route_name' => 'dopup.autocomplete_webform',
      '#default_value' => $this->configuration['webform'],
      '#required' => TRUE,
    ];
    if ($this->routeMatch->getParameter('block')) {
      $form['machine_name'] = [
        '#type' => 'hidden',
        '#default_value' => $this->routeMatch->getParameter('block')->id(),
      ];
      $form['#suffix'] = '<a href="/admin/config/system/dopup/' .
        $this->routeMatch->getParameter('block')->id() .
        '">Configure the popup settings</a>';
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['webform'] = $form_state->getValue('webform');
    $this->configuration['machine_name'] = $form_state->getValue('machine_name');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $configs = $this->configFactory->getEditable('dopup.settings')->get($this->configuration['machine_name']);
    if (empty($configs)) {
      $this->messenger()->addMessage(
        $this->t('You have not saved your Dopup Block configs. Go <a href=":url" target="_blank">HERE</a> and save your configs.',
          [':url' => '/admin/config/system/dopup/' . $this->configuration['machine_name']]
        )
      );
    }
    $cookie = $this->requestStack->getCurrentRequest()->cookies->get('dopup_' . $this->configuration['machine_name']);
    if ($cookie == NULL) {
      setcookie('dopup_' . $this->configuration['machine_name'], $configs[$this->configuration['machine_name'] . '_number'], time() + 3600);
    }
    $styles = '';
    $styles .= $this->colors();
    $styles .= '  .dopup__block {
      position: fixed;
      z-index: 10000;
      padding: 1rem 2rem;
    }';
    $styles .= $this->makeStyles();
    $styles .= $this->closeButton();
    $styles .= $this->getUserStyles();
    if ($configs[$this->configuration['machine_name'] . '_rounded_edges']) {
      $styles .= '
        .dopup__block.' . $this->configuration['machine_name'] . ' {
          border-radius: 10px;
        }
        .dopup__block.' . $this->configuration['machine_name'] . ' input,
        .dopup__block.' . $this->configuration['machine_name'] . ' select,
        .dopup__block.' . $this->configuration['machine_name'] . ' textarea {
          border-radius: 10px;
        }
      ';
    }
    else {
      $styles .= '
        .dopup__block.' . $this->configuration['machine_name'] . ' {
          border-radius: 0;
        }
        .dopup__block.' . $this->configuration['machine_name'] . ' input,
        .dopup__block.' . $this->configuration['machine_name'] . ' select,
        .dopup__block.' . $this->configuration['machine_name'] . ' textarea {
          border-radius: 0;
        }
      ';
    }
    return [
      '#theme' => 'dopup_block',
      '#attributes' => [
        'class' => ['dopup__block', $this->configuration['machine_name']],
        'data-machine-id' => $this->configuration['machine_name'],
      ],
      '#attached' => [
        'library' => [
          'dopup/dopup',
        ],
        'drupalSettings' => [
          'dopup' => [
            $this->configuration['machine_name'] => $configs,
          ],
        ],
      ],
      '#style' => $styles,
      '#webform' => $this->entityTypeManager->getViewBuilder('webform')
        ->view(
          $this->entityTypeManager->getStorage('webform')->load($this->configuration['webform'])
      ),
    ];
  }

  /**
   * Initialize colors.
   *
   * @return string
   *   String of colors in css variables.
   */
  protected function colors() {
    $configs = $this->configFactory->getEditable('dopup.settings')->get($this->configuration['machine_name']);
    return '
      :root {
        --' . $this->configuration['machine_name'] . '_text_color' . ':' . $configs[$this->configuration['machine_name'] . '_text_color'] . ';
        --' . $this->configuration['machine_name'] . '_link_color' . ':' . $configs[$this->configuration['machine_name'] . '_link_color'] . ';
        --' . $this->configuration['machine_name'] . '_shadow_color' . ':' . $configs[$this->configuration['machine_name'] . '_shadow_color'] . ';
        --' . $this->configuration['machine_name'] . '_bg_color' . ':' . $configs[$this->configuration['machine_name'] . '_bg_color'] . ';
        --' . $this->configuration['machine_name'] . '_input_field_color' . ':' . $configs[$this->configuration['machine_name'] . '_input_field_color'] . ';
        --' . $this->configuration['machine_name'] . '_input_field_bg_color' . ':' . $configs[$this->configuration['machine_name'] . '_input_field_bg_color'] . ';
        --' . $this->configuration['machine_name'] . '_heading_color' . ':' . $configs[$this->configuration['machine_name'] . '_heading_color'] . ';
      }
      .dopup__block.' . $this->configuration['machine_name'] . ' {
        text-align: ' . $configs[$this->configuration['machine_name'] . '_alignment'] . ';
      }
    ';
  }

  /**
   * Function to initialize general styles.
   *
   * @return string
   *   Styles string.
   */
  protected function makeStyles() {
    return '
      .dopup__block.' . $this->configuration['machine_name'] . ' {
        background-color: var(--' . $this->configuration['machine_name'] . '_bg_color);
        color: var(--' . $this->configuration['machine_name'] . '_text_color);
        box-shadow: 5px 10px 10px var(--' . $this->configuration['machine_name'] . '_shadow_color);
        display: none;
      }
      .dopup__block.' . $this->configuration['machine_name'] . ' a,
      .dopup__block.' . $this->configuration['machine_name'] . ' a:hover,
      .dopup__block.' . $this->configuration['machine_name'] . ' a:visited, {
        color: var(--' . $this->configuration['machine_name'] . '_link_color);
      }
      .dopup__block.' . $this->configuration['machine_name'] . ' input,
      .dopup__block.' . $this->configuration['machine_name'] . ' select,
      .dopup__block.' . $this->configuration['machine_name'] . ' textarea {
        background-color: var(--' . $this->configuration['machine_name'] . '_input_field_bg_color);
        color: var(--' . $this->configuration['machine_name'] . '_input_field_color);
        background-image: none;
      }
      .dopup__block.' . $this->configuration['machine_name'] . ' h1,
      .dopup__block.' . $this->configuration['machine_name'] . ' h2,
      .dopup__block.' . $this->configuration['machine_name'] . ' h3,
      .dopup__block.' . $this->configuration['machine_name'] . ' h4,
      .dopup__block.' . $this->configuration['machine_name'] . ' h5 {
        color: var(--' . $this->configuration['machine_name'] . '_heading_color);
      }
      .dopup__block.' . $this->configuration['machine_name'] . ' input:focus,
      .dopup__block.' . $this->configuration['machine_name'] . ' select:focus,
      .dopup__block.' . $this->configuration['machine_name'] . ' textarea:focus,
      .dopup__block.' . $this->configuration['machine_name'] . ' input:hover,
      .dopup__block.' . $this->configuration['machine_name'] . ' select:hover,
      .dopup__block.' . $this->configuration['machine_name'] . ' textarea:hover {
        outline: none !important;
        border: 1px solid var(--' . $this->configuration['machine_name'] . '_text_color);
      }
    ';
  }

  /**
   * Close button styles.
   *
   * @return string
   *   Close button style string.
   */
  protected function closeButton() {
    return '
      .dopup__block.' . $this->configuration['machine_name'] . ' .dopup-close.close-container {
        position: absolute;
        width: 30px;
        height: 30px;
        cursor: pointer;
        right: 0;
        top: 0;
        z-index: 10001;
      }

      .dopup__block.' . $this->configuration['machine_name'] . ' .dopup-close .leftright {
        height: 4px;
        width: 20px;
        position: absolute;
        margin-top: 24px;
        background-color: var(--' . $this->configuration['machine_name'] . '_text_color);
        border-radius: 2px;
        transform: rotate(45deg);
        transition: all .3s ease-in;
      }

      .dopup__block.' . $this->configuration['machine_name'] . ' .dopup-close .rightleft {
        height: 4px;
        width: 20px;
        position: absolute;
        margin-top: 24px;
        background-color: var(--' . $this->configuration['machine_name'] . '_text_color);
        border-radius: 2px;
        transform: rotate(-45deg);
        transition: all .3s ease-in;
      }

      .dopup__block.' . $this->configuration['machine_name'] . ' .dopup-close label {
        font-family: Helvetica, Arial, sans-serif;
        font-size: .6em;
        text-transform: uppercase;
        letter-spacing: 2px;
        transition: all .3s ease-in;
        opacity: 0;
      }

      .dopup__block.' . $this->configuration['machine_name'] . ' .dopup-close.close-container:hover .leftright {
        transform: rotate(-45deg);
        background-color: var(--' . $this->configuration['machine_name'] . '_link_color);
      }

      .dopup__block.' . $this->configuration['machine_name'] . ' .dopup-close.close-container:hover .rightleft {
        transform: rotate(45deg);
        background-color: var(--' . $this->configuration['machine_name'] . '_link_color);
      }

      .dopup__block.' . $this->configuration['machine_name'] . ' .dopup-close.close-container:hover label {
        opacity: 1;
      }
    ';
  }

  /**
   * Return user defined styles.
   *
   * @return string
   *   A string of user defined styles.
   */
  protected function getUserStyles() {
    $configs = $this->configFactory->getEditable('dopup.settings')->get($this->configuration['machine_name']);
    return '' . $configs[$this->configuration['machine_name'] . '_custom_styles'];
  }

}
