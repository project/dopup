<?php

namespace Drupal\dopup\Controller;

use Drupal\Component\Utility\Unicode;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Returns responses for Dopup routes.
 */
class DopupAutoCompleteController extends ControllerBase {


  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * To get the current request.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, RequestStack $requestStack) {
    $this->entityTypeManager = $entity_type_manager;
    $this->requestStack = $requestStack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('request_stack')
    );
  }

  /**
   * Builds the response.
   */
  public function build() {
    $keys = $this->requestStack->getCurrentRequest()->query->get('q');
    // @todo Filter by category as well.
    $webforms = $this->entityTypeManager->getStorage('webform')
      ->getQuery('OR')
      ->accessCheck(FALSE)
      ->condition('id', $keys, 'CONTAINS')
      ->condition('title', $keys, 'CONTAINS')
      ->condition('options', $keys, 'CONTAINS')
      ->execute();
    $webforms = array_map([$this, 'mapValueLabel'], array_values($webforms));
    return new JsonResponse($webforms);
  }

  /**
   * Map webform IDs array.
   *
   * Function to map webform ids to readable titles.
   *
   * @param string $webform
   *   Webform ID.
   *
   * @return array
   *   An array of value and label.
   */
  protected function mapValueLabel(string $webform) {
    return [
      'value' => $webform,
      'label' => str_replace('_', ' ', Unicode::ucfirst($webform)),
    ];
  }

}
