<?php

namespace Drupal\dopup\Form;

use Drupal\block\Entity\Block;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Configure Dopup settings for this site.
 */
class DopupSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'dopup_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['dopup.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Block $block = NULL) {
    if (empty($block)) {
      return new Response('Access Denied', Response::HTTP_FORBIDDEN);
    }
    $form['#block__id'] = $block->id();
    $config = $this->config('dopup.settings')->get($form['#block__id']);
    $form['#attributes']['autocomplete'] = 'off';
    $form['main'] = [
      '#type' => 'vertical_tabs',
      '#title' => $this->t('Popup Configuration'),
    ];
    $form['settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Settings'),
      '#group' => 'main',
    ];
    $form['settings'][$form['#block__id'] . '_position'] = [
      '#type' => 'select',
      '#options' => [
        'center' => $this->t('Center'),
        'left-bottom' => $this->t('Left Bottom'),
        'right-bottom' => $this->t('Right Bottom'),
        'top-right' => $this->t('Top Right'),
        'top-left' => $this->t('Top Left'),
        'custom' => $this->t('Custom'),
      ],
      '#title' => $this->t('Position of Popup'),
      '#empty_option' => '-- Select --',
      '#description' => $this->t('The position of the popup when it pops up.'),
      '#default_value' => empty($config[$form['#block__id'] . '_position']) ? 'center' : $config[$form['#block__id'] . '_position'],
      '#required' => TRUE,
    ];
    $form['settings'][$form['#block__id'] . '_custom_left'] = [
      '#type' => 'number',
      '#title' => $this->t('Position from left in percentage.'),
      '#default_value' => $config[$form['#block__id'] . '_custom_left'] ?? 1,
      '#states' => [
        'visible' => [
          ':input[name="' . $form['#block__id'] . '_position"]' => ['value' => 'custom'],
        ],
      ],
      '#field_suffix' => '%',
    ];
    $form['settings'][$form['#block__id'] . '_custom_top'] = [
      '#type' => 'number',
      '#title' => $this->t('Position from top in percentage.'),
      '#default_value' => $config[$form['#block__id'] . '_custom_top'] ?? 1,
      '#states' => [
        'visible' => [
          ':input[name="' . $form['#block__id'] . '_position"]' => ['value' => 'custom'],
        ],
      ],
      '#field_suffix' => '%',
    ];
    $form['settings'][$form['#block__id'] . '_when'] = [
      '#type' => 'select',
      '#options' => [
        'five' => $this->t('5 seconds after page load'),
        'now' => $this->t('As soon as page loads'),
        'bottom' => $this->t('When user scrolls to bottom'),
        'middle' => $this->t('When user scrolls to the middle of the page'),
        'exit' => $this->t('When user is about to exit the page'),
        'custom' => $this->t('Custom'),
      ],
      '#title' => $this->t('When to pop!'),
      '#description' => $this->t('When the popup will appear.'),
      '#default_value' => empty($config[$form['#block__id'] . '_when']) ? 'now' : $config[$form['#block__id'] . '_when'],
      '#empty_option' => '-- Select --',
      '#required' => TRUE,
    ];
    $form['settings'][$form['#block__id'] . '_custom_when'] = [
      '#type' => 'number',
      '#title' => $this->t('Time in seconds after which popup appears.'),
      '#default_value' => $config[$form['#block__id'] . '_custom_when'] ?? 10,
      '#states' => [
        'visible' => [
          ':input[name="' . $form['#block__id'] . '_when"]' => ['value' => 'custom'],
        ],
      ],
      '#description' => $this->t('If this is 10, then after 10 seconds popup will appear.'),
      '#field_suffix' => 'seconds',
    ];
    $form['settings'][$form['#block__id'] . '_no'] = [
      '#title' => $this->t('This feature is not allowed.'),
      '#type' => 'textarea',
      '#disabled' => TRUE,
      '#default_value' => $this->t("This is a really bad practice for marketing purposes. When a user wants to close a tab, he should be able to do so immediately. Please don't do this."),
      '#states' => [
        'visible' => [
          ':input[name="' . $form['#block__id'] . '_when"]' => ['value' => 'exit'],
        ],
      ],
    ];
    $form['settings'][$form['#block__id'] . '_custom_where'] = [
      '#type' => 'number',
      '#title' => $this->t('Position of page in percentage'),
      '#default_value' => $config[$form['#block__id'] . '_custom_where'] ?? 80,
      '#states' => [
        'visible' => [
          ':input[name="' . $form['#block__id'] . '_when"]' => ['value' => 'custom'],
        ],
      ],
      '#description' => $this->t('If this is 80, then when the user reads 80% of the page, the popup will appear.'),
      '#field_suffix' => '%',
    ];
    $form['settings'][$form['#block__id'] . '_rounded_edges'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Rounded Edges'),
      '#default_value' => $config[$form['#block__id'] . '_rounded_edges'] ?? FALSE,
      '#description' => $this->t('If this is checked, all edges will be rounded.'),
    ];
    $form['settings'][$form['#block__id'] . '_alignment'] = [
      '#type' => 'select',
      '#options' => [
        'left' => $this->t('Left'),
        'right' => $this->t('Right'),
        'center' => $this->t('Center'),
      ],
      '#title' => $this->t('Alignment'),
      '#default_value' => $config[$form['#block__id'] . '_alignment'] ?? 'left',
      '#description' => $this->t('The alignment of everything in the block, unless overridden in the webform.'),
    ];
    $form['settings'][$form['#block__id'] . '_number'] = [
      '#type' => 'number',
      '#title' => $this->t('Number of times to pop'),
      '#default_value' => $config[$form['#block__id'] . '_number'] ?? 3,
      '#description' => $this->t('The number of times the popup will appear on each page load after the user closes it. Default is 3.'),
    ];
    $form['colors'] = [
      '#type' => 'details',
      '#title' => $this->t('Colors'),
      '#group' => 'main',
    ];
    $form['colors'][$form['#block__id'] . '_text_color'] = [
      '#type' => 'color',
      '#title' => $this->t('Text Color'),
      '#default_value' => $config[$form['#block__id'] . '_text_color'] ?? '#055a8e',
    ];
    $form['colors'][$form['#block__id'] . '_heading_color'] = [
      '#type' => 'color',
      '#title' => $this->t('Heading Color'),
      '#default_value' => $config[$form['#block__id'] . '_heading_color'] ?? '#055a8e',
      '#description' => $this->t('The colors of h1, h2, h3, h4, h5'),
    ];
    $form['colors'][$form['#block__id'] . '_link_color'] = [
      '#type' => 'color',
      '#title' => $this->t('Link Color'),
      '#default_value' => $config[$form['#block__id'] . '_link_color'] ?? '#1d84c3',
    ];
    $form['colors'][$form['#block__id'] . '_bg_color'] = [
      '#type' => 'color',
      '#title' => $this->t('Background Color'),
      '#default_value' => $config[$form['#block__id'] . '_bg_color'] ?? '#fffeff',
    ];
    $form['colors'][$form['#block__id'] . '_shadow_color'] = [
      '#type' => 'color',
      '#title' => $this->t('Shadow Color'),
      '#default_value' => $config[$form['#block__id'] . '_shadow_color'] ?? '#055a8e',
    ];
    $form['colors'][$form['#block__id'] . '_input_field_color'] = [
      '#type' => 'color',
      '#title' => $this->t('Input Field Color (input, select, textarea)'),
      '#default_value' => $config[$form['#block__id'] . '_input_field_color'] ?? '#055a8e',
    ];
    $form['colors'][$form['#block__id'] . '_input_field_bg_color'] = [
      '#type' => 'color',
      '#title' => $this->t('Input Field Background Color (input, select, textarea)'),
      '#default_value' => $config[$form['#block__id'] . '_input_field_bg_color'] ?? '#fffeff',
    ];
    $form['custom'] = [
      '#type' => 'details',
      '#title' => $this->t('Custom Styles'),
      '#group' => 'main',
    ];
    $form['custom'][$form['#block__id'] . '_custom_styles'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Add your custom CSS here.'),
      '#default_value' => $config[$form['#block__id'] . '_custom_styles'] ?? '',
      '#rows' => 10,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = [
      $form['#block__id'] . '_position' => $form_state->getValue($form['#block__id'] . '_position'),
      $form['#block__id'] . '_custom_left' => $form_state->getValue($form['#block__id'] . '_custom_left'),
      $form['#block__id'] . '_custom_top' => $form_state->getValue($form['#block__id'] . '_custom_top'),
      $form['#block__id'] . '_when' => $form_state->getValue($form['#block__id'] . '_when'),
      $form['#block__id'] . '_custom_when' => $form_state->getValue($form['#block__id'] . '_custom_when'),
      $form['#block__id'] . '_custom_where' => $form_state->getValue($form['#block__id'] . '_custom_where'),
      $form['#block__id'] . '_text_color' => $form_state->getValue($form['#block__id'] . '_text_color'),
      $form['#block__id'] . '_link_color' => $form_state->getValue($form['#block__id'] . '_link_color'),
      $form['#block__id'] . '_bg_color' => $form_state->getValue($form['#block__id'] . '_bg_color'),
      $form['#block__id'] . '_shadow_color' => $form_state->getValue($form['#block__id'] . '_shadow_color'),
      $form['#block__id'] . '_input_field_color' => $form_state->getValue($form['#block__id'] . '_input_field_color'),
      $form['#block__id'] . '_input_field_bg_color' => $form_state->getValue($form['#block__id'] . '_input_field_bg_color'),
      $form['#block__id'] . '_custom_styles' => $form_state->getValue($form['#block__id'] . '_custom_styles'),
      $form['#block__id'] . '_rounded_edges' => $form_state->getValue($form['#block__id'] . '_rounded_edges'),
      $form['#block__id'] . '_alignment' => $form_state->getValue($form['#block__id'] . '_alignment'),
      $form['#block__id'] . '_number' => $form_state->getValue($form['#block__id'] . '_number'),
      $form['#block__id'] . '_heading_color' => $form_state->getValue($form['#block__id'] . '_heading_color'),
    ];
    $this->config('dopup.settings')
      ->set($form['#block__id'], $config)
      ->save();
    parent::submitForm($form, $form_state);
  }

}
